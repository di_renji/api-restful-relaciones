package com.org.Bidireccional.Repository;

import com.org.Bidireccional.Entity.Libro;
import org.springframework.data.jpa.repository.JpaRepository;


public interface LibroRepository extends JpaRepository<Libro, Integer> {
}
