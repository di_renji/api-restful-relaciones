package com.org.Bidireccional.Repository;

import com.org.Bidireccional.Entity.Biblioteca;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BibliotecaRepository extends JpaRepository<Biblioteca, Integer> {
}
