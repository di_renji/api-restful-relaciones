package com.org.Bidireccional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiResTfulBidireccionalApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiResTfulBidireccionalApplication.class, args);
	}

}
