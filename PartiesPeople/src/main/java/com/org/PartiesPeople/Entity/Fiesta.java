package com.org.PartiesPeople.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tbl_fiesta")
public class Fiesta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fiesta_id")
    private Long id;

    private String ubicacion;

    @JsonFormat(pattern = "YYYY-MM-dd")
    private Date fecha;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "tbl_personas_fiestas",
            joinColumns = @JoinColumn(name = "fiesta_id", referencedColumnName = "fiesta_id"),
            inverseJoinColumns = @JoinColumn(name = "persona_id", referencedColumnName = "persona_id"))
    private Set<Persona> personas = new HashSet<>();

}
