package com.org.PartiesPeople.Controller;

import com.org.PartiesPeople.Entity.Fiesta;
import com.org.PartiesPeople.Entity.Persona;
import com.org.PartiesPeople.Repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api/persona")
public class PersonaControler {

    @Autowired
    private PersonaRepository personaRepository;

    @GetMapping("/listar")
    public ResponseEntity<Collection<Persona>> listarPersonas(){
        return new ResponseEntity<>(personaRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/buscar/{id}")
    public ResponseEntity<Persona> obtenerPersonaPorId(@PathVariable Long id){
        Persona persona = personaRepository.findById(id).orElseThrow();
        if (persona != null){
            return new ResponseEntity<>(personaRepository.findById(id).orElseThrow(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/registrar")
    public ResponseEntity<?> guardarPersona(@RequestBody Persona persona){
        return new ResponseEntity<>(personaRepository.save(persona), HttpStatus.CREATED);
    }

    @DeleteMapping("/eliminar")
    public ResponseEntity<Void> eliminarPersona(@PathVariable Long id){
        personaRepository.deleteById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @GetMapping("/{id}/fiestas")
    public ResponseEntity<Collection<Fiesta>> listarFiestasDeLaPersona(@PathVariable Long id){
        Persona persona = personaRepository.findById(id).orElseThrow();
        if (persona != null){
            return new ResponseEntity<>(persona.getFiestas(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }


}
