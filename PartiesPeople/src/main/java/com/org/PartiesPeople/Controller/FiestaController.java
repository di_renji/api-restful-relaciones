package com.org.PartiesPeople.Controller;

import com.org.PartiesPeople.Entity.Fiesta;
import com.org.PartiesPeople.Repository.FiestaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api/fiesta")
public class FiestaController {

    @Autowired
    private FiestaRepository fiestaRepository;

    @GetMapping("/listar")
    public ResponseEntity<Collection<Fiesta>> listarFiestas(){
        return new ResponseEntity<>(fiestaRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/buscar/{id}")
    public ResponseEntity<Fiesta> obtenerFiestaPorId(@PathVariable Long id){
        Fiesta fiesta = fiestaRepository.findById(id).orElseThrow();
        if (fiesta != null){
            return new ResponseEntity<>(fiestaRepository.findById(id).orElseThrow(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/registrar")
    public ResponseEntity<?> guardarFiesta(@RequestBody Fiesta fiesta){
        return new ResponseEntity<>(fiestaRepository.save(fiesta), HttpStatus.CREATED);
    }

    @DeleteMapping("/eliminar")
    public ResponseEntity<Void> eliminarFiesta(@PathVariable Long id){
        fiestaRepository.deleteById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }


}
