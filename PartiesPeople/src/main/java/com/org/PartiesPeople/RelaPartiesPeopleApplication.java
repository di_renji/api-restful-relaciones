package com.org.PartiesPeople;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelaPartiesPeopleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RelaPartiesPeopleApplication.class, args);
	}

}
